const mongoose = require('mongoose');

const user = 'symkach';
const password = 'Ztrctkrj22'
const db = 'pinpoint';

const uri =`mongodb+srv://${user}:${password}@cluster0-flocn.mongodb.net/${db}?retryWrites=true&w=majority`;

// const client = new MongoClient(uri, { useNewUrlParser: true});
mongoose.connect(uri, {useNewUrlParser: true});

const PricesSchema = new mongoose.Schema({
    product_uid:  mongoose.Schema.Types.ObjectId,
    material_uid: String,
    name: String,
    values: [
        {
            params: [{name: String, value: String}],
            prices: [{name: String, value: String}],
            types: [{name: String, value: String}]
        }
    ],
    fields: [String],
    titles: [String],
    myob: Array
})

module.exports = mongoose.model('Prices', PricesSchema);
