const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer();

const app = express();

const apiRouter = express.Router();

const MaterialModel = require('./Material.model');
const ProductModel = require('./Product.model');
const PricesModel = require('./Prices.model');

apiRouter.post('/materials', (req, res) => {
	const { body } = req;
	console.log(body);
	if (!body || Object.entries(body).length === 0) {
		return res.status(400).send('No request body');
	}
	const model = new MaterialModel(body);
	model.uid = model._id;
	model.save()
		.then(doc => {
			if(!doc || doc.length === 0) {
					return res.status(500).send(doc);
			}
				return res.status(200).send(doc);
		})
		.catch(err => {
				return res.status(500).send(err)
		})
})

apiRouter.get('/materials', (req, res) => {
	const { search } = req.query || '';
	MaterialModel.find({name: { $regex: search}})
		.then(materials => {
			return res.status(200).send(materials);
		})
})

apiRouter.get('/materials/:uid', (req, res) => {
	const { uid } = req.params;
	MaterialModel.findOne({uid})
		.then(material => {
			if (!material || !Object.entries(material).length) {
				return res.status(400).send('Not found');
			}
			return res.status(200).send(material);
		})
		.catch(err => {
			return res.status(500).send(err);
		})
})


apiRouter.put('/materials/:uid', async (req, res) => {
	const { uid } = req.params;
	MaterialModel.findOneAndUpdate({uid}, req.body)
		.then(material => {
			return res.status(200).send(req.body);
		})
		.catch(err => {
			return res.status(500).send(err);
		})
});

apiRouter.post('/products', upload.none(), (req, res) => {
	const { body } = req;
	// console.log(JSON.parse(body.json));
	if (!body || Object.entries(body).length === 0) {
		return res.status(400).send('No request body');
	}
	const parsedBody = JSON.parse(body.json);
	const newProduct = {...parsedBody, tables: parsedBody.tables}
	console.log(parsedBody)
	const model = new ProductModel({...parsedBody, tables: []});
	PricesModel.create(parsedBody.tables.map(item => ({...item, product_uid: model._id})), (err, arr) => {
		console.log(err);
		model.uid = model._id;
		model.category = 'Miscellaneous';
		model.tables = arr.map(item => item.id);
		model.save()
			.then(doc => {
				if(!doc || doc.length === 0) {
						return res.status(500).send(doc);
				}
				return res.status(200).send(doc);
			})
			.catch(err => {
				console.log(err);
				return res.status(500).send(err)
			})
	})
})

apiRouter.get('/products', (req, res) => {
	const { search = '' } = req.query;
	ProductModel.find({name: { $regex: search}})
		.populate('tables')
		.then(products => {
			return res.status(200).send(products);
		})
})

apiRouter.get('/products/:uid', (req, res) => {
	const { uid } = req.params;
	ProductModel.findOne({uid})
		.populate('tables')
		.then(product => {
			if (!product || !Object.entries(product).length) {
				return res.status(400).send('Not found');
			}
			return res.status(200).send(product);
		})
		.catch(err => {
			return res.status(500).send(err);
		})
})

apiRouter.put('/products/:uid', upload.none(), async (req, res) => {
	const { uid } = req.params;
	ProductModel.findOneAndUpdate({uid}, {...JSON.parse(req.body.json), category: 'Miscellaneous'})
		.then(() => {
			return res.status(200).send(req.body);
		})
		.catch(err => {
			return res.status(500).send(err);
		})
});

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader('Access-Control-Allow-Methods', '*');
  res.setHeader("Access-Control-Allow-Headers", "*");
  next();
});

app.use(bodyParser.json())
app.use('/api/v1', apiRouter);

app.listen(3000, () => {
	console.log('Server started;')
})